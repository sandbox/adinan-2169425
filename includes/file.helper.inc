<?php

/**
 * @file
 * Contains the CustomizedLogoFileHelper class.
 */

/**
 * Provides methods for dealing with files.
 */
class CustomizedLogoFileHelper {

  /**
   * Saves the file in the files system.
   *
   * @param int $fid
   */
  public static function saveFile($fid) {
    $file = file_load($fid);
    // Change status to permanent.
    $file->status = FILE_STATUS_PERMANENT;
    // File save.
    file_save($file);
    // Record that the module is using the file.
    file_usage_add($file, 'customized_logo', 'customized_logo_block', 1);
  }

  /**
   * Deletes the file from the disk.
   *
   * @param int $fid
   */
  public static function deleteFile($fid) {
    $file = file_load($fid);
    file_usage_delete($file, 'customized_logo');
    file_delete($file, $force = TRUE);
  }

}
