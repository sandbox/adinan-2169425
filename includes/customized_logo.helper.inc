<?php

/**
 * @file
 * Contains the LogoBlockViewHelper class.
 */

/**
 * Helper functions that are used to show the correct logo in the logo block.
 */
class CustomizedLogoHelper {

  /**
   * Minimizes code in the hook_block_save().
   *
   * @param array $settings
   * @param array $old_settings
   * @param int $iterator
   */
  public static function setArray(&$settings, $old_settings, $iterator) {

    $fields = array('file', 'start_date', 'end_date', 'title', 'alt');

    if (is_array($old_settings)) {
      foreach ($fields as $field) {
        $settings['logo' . $iterator . '_' . $field] = $old_settings['logo' . $iterator . '_' . $field];
      }
    }
    else {
      foreach ($fields as $field) {
        $settings['logo' . $iterator . '_' . $field] = '';
      }
    }
  }

  /**
   * Checks which logo is active and returns the coreponding parameters.
   *
   * @return array $template_params
   */
  public static function getActiveLogoParams() {
    $now = time();
    $settings = LazyVars::get('logo_settings');

    for ($i = 1; $i <= CUSTOMIZED_LOGO_MAX_IMAGES; $i++) {
      $active_conditions = !empty($settings['logo' . $i . '_file']) && !empty($settings['logo' . $i . '_start_date']) && !empty($settings['logo' . $i . '_end_date']) && ($now > strtotime($settings['logo' . $i . '_start_date'])) && ($now < strtotime($settings['logo' . $i . '_end_date']));

      if ($active_conditions) {
        $active_logo_no = $i;
      }
    }
    if (isset($active_logo_no)) {
      $uri = file_load($settings['logo' . $active_logo_no . '_file'])->uri;
      $path = file_create_url($uri);
      $title = $settings['logo' . $active_logo_no . '_title'];
      $alt = $settings['logo' . $active_logo_no . '_alt'];

      $template_params = array(
        'path' => $path,
        'title' => $title,
        'alt' => $alt,
      );
    }
    elseif (!empty($settings['default_file'])) {
      $uri = file_load($settings['default_file'])->uri;
      $path = file_create_url($uri);
      $site_name = variable_get('site_name', '');

      $template_params = array(
        'path' => $path,
        'title' => $site_name,
        'alt' => $site_name,
      );
    }
    else {
      $site_name = variable_get('site_name', '');
      if (!$path = theme_get_setting('logo_path')) {
        $path = theme_get_setting('logo');
      }

      $template_params = array(
        'path' => $path,
        'title' => $site_name,
        'alt' => $site_name,
      );
    }
    return $template_params;
  }

  /**
   * Checks if the entered data ranges overlap.
   *
   * @param array $edit
   *
   * @return bool $overlap
   */
  public static function rangesOverlap($edit) {
    $overlap = FALSE;
    for ($i = 1; $i <= CUSTOMIZED_LOGO_MAX_IMAGES; $i++) {
      $start_date = strtotime($edit['logo' . $i . '_start_date']);
      $end_date = strtotime($edit['logo' . $i . '_end_date']);

      for ($j = $i + 1; $j <= CUSTOMIZED_LOGO_MAX_IMAGES; $j++) {

        $start_date2 = strtotime($edit['logo' . $j . '_start_date']);
        $end_date2 = strtotime($edit['logo' . $j . '_end_date']);

        $overlap_condition = ($start_date < $end_date2 &&
            $end_date > $start_date2);
        if ($overlap_condition) {
          $overlap = TRUE;
          break;
        }
      }
    }
    return $overlap;
  }

  /**
   * Checks if the end_date is later in time that the start_date.
   *
   * @param string $start_date
   * @param string $end_date
   *
   * @return bool
   */
  public static function correctRange($start_date, $end_date) {
    $start_date = strtotime($start_date);
    $end_date = strtotime($end_date);

    if ($start_date <= $end_date) {
      return TRUE;
    }
    return FALSE;
  }

}
