/**
 * @file
 * Contains the script that enables adding and removing logo entries
 * and adds the calendar datepicker to the date fields.
 */

(function($) {
  $(document).ready(function($) {

    var hidden = new Array();
    var visible = new Array();
    for (i = 1; i <= 10; i++) {
      if ($("#edit-logo" + i + " .datepick").val() === '') {
        $("#edit-logo" + i).hide();
        hidden.push("#edit-logo" + i);
      }
      else {
        visible.push("#edit-logo" + i);
      }
    }

    $('#edit-add-logo-option').live("click", function(e) {
      e.preventDefault();


      var id = hidden.shift();
      $(id).show();
      visible.push(id);
      visible.sort(function(a, b) {
        a = a.replace('#edit-logo', '');
        b = b.replace('#edit-logo', '');
        return a - b;
      });
      return false;

    });

    $('.remove-button').live('click', function(e) {
      e.preventDefault();
      // Creates the id of the fieldset for the entry to be removed.
      var id = $(this).attr("id").replace('-remove-option', '');
      id = '#' + id;
      $(id + ' .datepick').val('');
      $(id + ' .logo-title').val('');
      $(id + ' .logo-alt').val('');
      $(id).hide();
      hidden.push(id);
      var index = visible.indexOf(id, 0);
      visible.splice(index, 1);
      hidden.sort(function(a, b) {
        a = a.replace('#edit-logo', '');
        b = b.replace('#edit-logo', '');
        return a - b;
      });
      return false;
    });

    $(".datepick").datepicker({
      dateFormat: "yy-mm-dd",
      autoSize: true
    });
  });

}(jQuery));
