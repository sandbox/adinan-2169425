<?php

/**
 * @file
 * Theme callback functions for Customized Logo
 */

/**
 * Theme wrapper for customized logo block.
 */
function theme_customized_logo_block_wrapper($variables) {
  $link = '<front>';
  if ($link) {
    return l($variables['element']['#children'], $link, array(
      'absolute' => TRUE,
      'html' => TRUE));
  }
  return $variables['element']['#children'];
}
